### Deathstrider Ammunition eXpansion
---
This is an addon for Deathstrider that serves as an example to show others how to create and add their own custom ammunition to Deathstrider and also as a community project to collect all the ammunition types that have been added into one singular library.

### Rules and Guidelines
---
Any ammunition that you create you are free to submit to be added to DAX, however, I will not accept or add any meme or overtly overpowered ammunition. Anything submitted must be balanced around the existing game balance. For example: making a 5.56 bullet that creates a nuclear explosion would be something that is both a meme and overpowered so obviously that wouldn't be accepted. Making a 5.56 bullet that deals more damage than a typical 5.56 bullet but is more rare, expensive and heavier would be acceptable.

### Color Coordination
---
Most rounds when loaded into a magazine follow the following color coordination:
- Red: Indicates a round which typically deals higher damage to monsters at the expense of lower damage to shields.
- Blue: Rounds which typically deal higher damage to shields but lower damage to flesh. Some of the higher calibers can penetrate.
- Green: A utility round which has secondary effects which make it more potent than it appears to be. Usually something like an incendiary effect.
- Yellow: A round which specializes in single target damage. Typically applies to rockets or explosive ammunition.

This is not an absolute guideline. You are free to create your own custom colors for your ammunition. This is just the color coordination that I chose to follow because it closely resembled the vanilla HUD coloring.

### Current Ammo Types
---
Currently there are 10 new ammo types available.
- .50 AE Hollow Point: Deals additional damage to unshielded enemies. Deals significantly less damage to shields.
- 12ga Dragon's Breath: Deals incendiary damage to enemies. Deals significantly less damage to shields.
- 5.56 Armor Piercing: Deals additional damage to shields. Slightly lower overall damage to unshielded enemies.
- 5.56 Incendiary: Deals incendiary damage to enemies. Deals significantly less damage to shields.
- 7.62 Armor Piercing: Deals additional damage to shields. Penetrates enemies. Slightly lower overall damage to unshielded enemies.
- 7.62 Incendiary: Deals incendiary damage to enemies. Deals significantly less damage to shields.
- .50 BMG Saboted Light Armor Penetrator (SLAP): Deals extreme damage to shields. Penetrates multiple enemies. Slightly lower overall damage to unshielded enemies.
- .50 BMG High Explosive Incendiary: Explodes on contact and deals incendiary damage to enemies. Deals less damage to shields.
- High Explosive Dual Purpose (HEDP) Rocket Grenades: Extreme single target damage. Useful for picking out a single high value target out of a group.
- White Phosphorus Rocket Grenades: The war crime rocket. Unleashes a toxic cloud of incendiary smoke. Causes the player to gain evil alignment. Really good for clearing out rooms.

### Credits and Permissions
---
All the cool people who helped me make this mod:
- Accensus, coding
- HalfBakedCake, coding
- Pillowblaster, base sprites for the Rocket Grenades, 5.56 box, Shotgun shell box and .50 AE box
- Popguy12, base sprites for the 10mm box
- Icarus, base sprites for the 7.62 box and .50 BMG box, edits to all the original sprites. (Yes, I'm crediting myself. Yes, I have a massive ego.)

All the assets used in this mod are used with permission from the original creators, if you want to use any of the assets in this mod in your own content you must get permission from the orginal creator.