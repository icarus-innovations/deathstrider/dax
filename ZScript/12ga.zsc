////////////////////////////
//12 gauge Dragon's Breath//
////////////////////////////

class DSP_12DB : DSProjectile
{
	override void PostBeginPlay()
	{
		MaxTravelDistance *= frandom(1.0, 1.4);
		Super.PostBeginPlay();
	}

	override void OnHitActor(Actor target)
	{
		target.GiveInventory('DSHeat', random(5, 10));
		Super.OnHitActor(target);
	}

	override void OnMove(Vector3 dir, double distTravelled, double distFromStart)
	{
		dir *= -1;
		for (int i = 0; i < distTravelled; i += 4)
		{
			double roll = random(0, 359);
			double colFac = frandom(0.3, 1.2);
			A_SpawnParticle(DSCore.MultiplyColor(0xFFEE44, 1.0, colFac, colFac * 0.5), SPF_FULLBRIGHT | SPF_NOTIMEFREEZE, 10, frandom(0.5, 0.75), Angle,
				i * dir.X + frandom(-1.0, 1.0),
				i * dir.Y + frandom(-1.0, 1.0) * cos(Roll),
				i * dir.Z + frandom(-1.0, 1.0) * sin(Roll),
				frandom(-0.1, 0.1),
				frandom(-0.1, 0.1),
				-0.15);
		}
		Super.OnMove(dir, distTravelled, distFromStart);
	}

	Default
	{
		Speed DSCONST_MPSTODUPT * 200;
		DamageFunction random(5, 10);
		Mass 25;
		Obituary "%o may or may not have been victim to a war crime.";
		+DSPROJECTILE.NORICOCHET
		DSProjectile.MaxTravelDistance DSCONST_ONEMETER * 6;
	}
}

class DSSpent12DB : DSSpentBuckshot
{
	States
	{
		Spawn:
			DBEC ABCDEFGH 2;
			Loop;
	}
}

class DS12DBAmmo : DSBuckshotAmmo
{
	override void SetProperties()
	{
		Super.SetProperties();
		AmmoProps.SpreadFactor = 8.0;
	}
	
	override double GetWeight(DSData data) { return 0.05; }
	override string GetIcon(DSData data) { return "12DBX0"; }
	override string GetTechnicalInfo(DSData data) { return "- Multiple Incendiary projectiles\n\c[Green]- Burns at extreme temperatures\n\c[Green]- Lights enemies on fire\n\c[Yellow]- Low spread\n\c[Red]- Low damage\n\c[Red]- Pathetic shield damage"; }
	override string GetLore(DSData data) { return "Dragon's breath consists primarily of a single magnesium core. When the round is fired, sparks and flames can shoot out to 20 meters. The safety aspects of the ammunition are disputed, as the magnesium shards burn at approximately 2,000 °C, which is more than enough to light a person, or building, on fire. Its tactical usage remains undocumented."; }
	override Color, string GetAmmoColor()
	{
		return 0xFF224B1B, "Dark Green";
	}

	Default
	{
		XScale 0.25;
		YScale 0.25;
		Tag "12 ga Dragon's Breath";
		Inventory.Icon "12DBX0";
		DSAmmo.Projectile 'DSP_12DB', 36;
		DSRoundAmmo.Casing 'DSSpent12DB';
		DSAmmo.Stack 4, 12;
		DSItem.ModId 'IcarusInnovations';
		+DSITEM.MULTIPICKUP
	}
	
	States
	{
		Spawn:
			12DB A -1;
			12DB B -1 A_SetScale(0.32);
			12DB X -1 A_SetScale(0.3);
			Stop;
	}
}

class DS12DBPack : DSAmmoSpawner
{
	Default
	{
		DSAmmoSpawner.Properties 'DS12DBAmmo', 4;
	}
}

class DS12DBBox : DSAmmoSpawner
{
	Default
	{
		DSAmmoSpawner.Properties 'DS12DBAmmo', 12;
	}
}

class DS12DBAmmo_Store : DSStoreItem
{
	override double GetCost() { return 1.40; }
	override double, int GetResupplyAmount() { return 12, 48; }
}